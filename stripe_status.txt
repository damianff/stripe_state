
    private function getStripeAccountStatus(Account $account): StripeAccountStatus
    {
        if ($account->payouts_enabled == false) {
            if (!empty($account->requirements->past_due)) {
                return StripeAccountStatus::RESTRICTED();
            } else {
                return StripeAccountStatus::PENDING();
            }
        } else {
            if (empty($account->requirements->currently_due) && empty($account->requirements->eventually_due)) {
                return StripeAccountStatus::COMPLETED();
            } else {
                if (!empty($account->requirements->currently_due)) {
                    return StripeAccountStatus::RESTRICTED_SOON();
                } else {
                    return StripeAccountStatus::ENABLED();
                }
            }
        }
    }
